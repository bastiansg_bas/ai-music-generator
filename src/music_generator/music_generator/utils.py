import os
import logging

import numpy as np
import magenta.music as mm

from tensor2tensor.data_generators import text_encoder
from magenta.models.score2perf import score2perf
from base64 import b64encode


logger = logging.getLogger(__name__)


PRIMER_MAX_SECONDS = os.getenv('PRIMER_MAX_SECONDS', 20)


class PianoPerformanceLanguageModelProblem(score2perf.Score2PerfProblem):
    @property
    def add_eos_symbol(self):
        return True


class MelodyToPianoPerformanceProblem(score2perf.AbsoluteMelody2PerfProblem):
    @property
    def add_eos_symbol(self):
        return True


def decode(ids, encoder):
    ids = list(ids)
    if text_encoder.EOS_ID in ids:
        ids = ids[:ids.index(text_encoder.EOS_ID)]
    return encoder.decode(ids)


def unconditional_input_generator(targets, decode_length):
    while True:
        yield {
            'targets': np.array([targets], dtype=np.int32),
            'decode_length': np.array(decode_length, dtype=np.int32)
        }


def melody_input_generator(inputs, decode_length):
    while True:
        yield {
            'inputs': np.array([[inputs]], dtype=np.int32),
            'targets': np.zeros([1, 0], dtype=np.int32),
            'decode_length': np.array(decode_length, dtype=np.int32)
        }


def get_primer_ns(midi_string: str):
    primer_ns = mm.midi_to_note_sequence(midi_string)
    primer_ns = mm.apply_sustain_control_changes(primer_ns)

    if primer_ns.total_time > PRIMER_MAX_SECONDS:
        logger.warning(f'Primer duration {primer_ns.total_time} is \
            longer than max second {PRIMER_MAX_SECONDS}, truncating.')
        primer_ns = mm.extract_subsequence(primer_ns, 0, PRIMER_MAX_SECONDS)

    for note in primer_ns.notes:
        note.instrument = 1
        note.program = 0

    return primer_ns


def get_melody_ns(midi_string: str):
    melody_ns = mm.midi_to_note_sequence(midi_string)
    melody_instrument = mm.infer_melody_for_sequence(melody_ns)
    notes = filter(
        lambda x: x.instrument == melody_instrument, melody_ns.notes)

    notes = list(notes)
    del melody_ns.notes[:]
    melody_ns.notes.extend(
        sorted(notes, key=lambda note: note.start_time))

    for i in range(len(melody_ns.notes) - 1):
        melody_ns.notes[i].end_time = melody_ns.notes[i + 1].start_time

    return melody_ns


def encode_midi(midi_path: str) -> str:
    with open(midi_path, 'rb') as f:
        content = f.read()
        generated_midi = b64encode(content)
        generated_midi = generated_midi.decode('utf-8')
        return generated_midi
